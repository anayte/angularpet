import { ChangeDetectionStrategy } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
// import { timeStamp } from 'console';
import { Pet } from 'src/app/services/pet/pet.interface';
import { PetService } from '../services/pet/pet.service';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.css']
})
export class PetComponent implements OnInit {

  pet: Pet; 
  pets: Pet[];
  constructor( private petService: PetService) { 
    this.pet = {} as Pet;
    this.pets = [] as Pet[];
  }

  ngOnInit(): void {
    this.petService.getPets().subscribe(data => {
      this.pets = [];
      data.forEach( pet => {
        console.log('data: ', pet.payload.doc.data())
        this.pets.push(pet.payload.doc.data() as Pet)
      })
    })
  }

  createPet(){
    this.pet.notes = [{ note: ' Esta es una nota '}, { note: ' Esto es otra nota'}];
    this.pet.createdBy = "Anayte";
    this.pet.createdAt = Date();
    this.pet.status = true;
    this.petService.addPets(this.pet).then(() => 
    {
    alert("La mascota fue registrada");
    })
    console.log('pet: ', this.pet)
  }

}
