import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PetComponent } from './pet/pet.component';
import { OwnnerComponent } from './ownner/ownner.component';

const routes: Routes = [
  {path: 'pet', component: PetComponent},
  {path: 'ownner',component: OwnnerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
