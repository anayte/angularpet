import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Pet} from './pet.interface';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private firestore : AngularFirestore) 
  {
    
   }

  getPets () 
  {
    return this.firestore.collection('pet').snapshotChanges();
  }

  addPets (pet: Pet) 
  {
    return this.firestore.collection('pet').add(pet);
  }

  updatePets (pet: Pet) 
  {
    return this.firestore.doc(pet.uuId).update(pet);
  }
}
