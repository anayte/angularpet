import { TestBed } from '@angular/core/testing';

import { OwnnerService } from './ownner.service';

describe('OwnnerService', () => {
  let service: OwnnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OwnnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
