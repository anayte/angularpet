import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { OwnnerComponent } from 'src/app/ownner/ownner.component';
import { Ownner } from './ownner.interface';

@Injectable({
  providedIn: 'root'
})
export class OwnnerService {

  constructor(private firestore : AngularFirestore) 
  {
    
   }

  getOwnner () 
  {
    return this.firestore.collection('ownner').snapshotChanges();
  }

  addOwnner (ownner: Ownner) 
  {
    return this.firestore.collection('ownner').add(ownner);
  }

  updateOwnner (ownner: Ownner) 
  {
    return this.firestore.doc(ownner.uuId).update(ownner);
  }

}
