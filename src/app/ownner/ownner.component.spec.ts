import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnnerComponent } from './ownner.component';

describe('OwnnerComponent', () => {
  let component: OwnnerComponent;
  let fixture: ComponentFixture<OwnnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
