import { ChangeDetectionStrategy } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
// import { timeStamp } from 'console';
import { Ownner } from 'src/app/services/ownner/ownner.interface';
import { OwnnerService } from 'src/app/services/ownner/ownner.service';


@Component({
  selector: 'app-pet',
  templateUrl: './ownner.component.html',
  styleUrls: ['./ownner.component.css']
})
export class OwnnerComponent implements OnInit {

  ownner: Ownner;
  ownners: Ownner[];
  constructor( private ownnerService: OwnnerService) { 

    this.ownner = {} as Ownner;
    this.ownners = [] as Ownner[];

  }

  ngOnInit(): void {
    this.ownnerService.getOwnner().subscribe(data => {
      this.ownners = [];
      data.forEach( ownner => {
        console.log('data: ', ownner.payload.doc.data())
        this.ownners.push(ownner.payload.doc.data() as Ownner)
      })
    })
  }

  createOwnner(){
    this.ownner.createdAt = Date();
    this.ownner.status = true;
    this.ownnerService.addOwnner(this.ownner).then(() => 
    {
      alert("La mascota fue registrada");
    })
    console.log('Duenio: ', this.ownner)
  }


}
