// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA1XfnishmE_0IxjeD6B3qzE3T-mjSNTEk",
    authDomain: "snap-pet-9c3c2.firebaseapp.com",
    projectId: "snap-pet-9c3c2",
    storageBucket: "snap-pet-9c3c2.appspot.com",
    messagingSenderId: "659469241714",
    appId: "1:659469241714:web:4757ec33b8705b37fc21eb",
    measurementId: "G-G8X7HDHJXS"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
